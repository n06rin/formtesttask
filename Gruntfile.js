module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
      compileCore: {
        options: {
          strictMath: true,
          sourceMap: false,
          outputSourceFiles: false,
          //sourceMapURL: '<%= pkg.name %>.css.map',
          //sourceMapFilename: 'dist/css/<%= pkg.name %>.css.map'
        },
        src: 'src/less/bootstrap.less',
        dest: 'www/css/style.css'
      }
    },
    copy: {
      fonts: {
        expand: true,
        cwd: 'src/fonts/',
        src: '**',
        dest: 'www/fonts/'
      },
      /*
      imgs: {
        expand: true,
        cwd: 'src/img/',
        src: '**',
        dest: 'www/img/'
      },
      */
      html: {
        expand: true,
        cwd: 'src/',
        src: '**/*.html',
        dest: 'www/'
      },
      php: {
        expand: true,
        cwd: 'src/',
        src: '**/*.php',
        dest: 'www/'
      },
      /*
      js:{
        expand: true,
        cwd: 'src/js/',
        src: '**',
        dest: 'www/js/'
      }
      */
    },
    watch: {
        html: {
          files: 'src/*.html',
          tasks: 'copy:html'
        },
        php: {
          files: 'src/*.php',
          tasks: 'copy:php'
        },
        less: {
          files: 'src/less/*.less',
          tasks: 'less'
        }
      }

    });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['less:compileCore', 'copy']);

};